export const api = async (url, method = "GET", body = null, headers = {}) => {
    let req = await fetch(url, {
        method: method,
        body: body,
        headers: {
            'Accept': 'application/json',
            ...headers
        }
    })
   
 
    
    const res = await req.json()
    return {
        data: res,
        status: req.status
    }
}