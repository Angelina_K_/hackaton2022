import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,
  },
  getters: {
    getToken: state => state.token
  },
  mutations: {
    setToken(state, payload){
      state.token = payload
    },
  },
  actions: {
    async asyncSetToken({commit}, payload){
      await commit("setToken", payload)
    }
  },
  modules: {
  }
})
