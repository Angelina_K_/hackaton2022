import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/Registration',
    name: 'Registration',
    component: () => import ('@/views/Registration.vue')
  },
  {
    path: '/Sign_in',
    name: 'Sign_in',
    component: () => import ('@/views/Sign_in.vue')
  },
  {
    path: '/HomeUser',
    name: 'HomeUser',
    component: () => import ('@/views/HomeUser.vue')
  },
  {
    path: '/currencies1',
    name: 'currencies1',
    component: () => import ('@/views/currencies1.vue')
  },
  {
    path: '/currencies',
    name: 'currencies',
    component: () => import ('@/views/currencies.vue')
  },

  {
    path: '/transaction',
    name: 'transaction',
    component: () => import ('@/views/transaction.vue')
  },
  {
    path: '/wallet',
    name: 'wallet',
    component: () => import ('@/views/wallet.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
